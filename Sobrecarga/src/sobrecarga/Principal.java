
package sobrecarga;

/**
 *
 * @author joseo
 */
public class Principal {
    public static void main(String[] args) {
        // TODO code application logic here
        //System.out.println("==> CalculadoraInt --> resultado = "+ Principal.engine().calculate(5, 5));
        //System.out.println("==> CalculadoraLong --> resultado = "+ Principal.engine().calculate(6, 2));
        System.out.println("==> CalculadoraLong --> resultado = " + engine ((int)5, (int)5).calculate(5,5));
        System.out.println("==> CalculadoraLong --> resultado = " + engine ((long)6, (long)2).calculate(6,4));
        System.out.println("==> CalculadoraLong --> resultado = " + engine ((long)6, (long)2).calculate(6,4));  
        
    }
    private static CalculadoraInt engine(int a, int b){
        //int resultado = cal.calcular(x,y);
        return (x, y) -> a*b;
    }
    
    private static CalculadoraLong engine(long a, long b){
        return (x, y) -> x-y;
    }
    
}
